What This Does
--------------
This is a small bot that will send messages to a [matrix](https://matrix.org/) room 
about upcoming rocket launches. 
The data comes from [here](https://thespacedevs.com/llapi).

It will look like this:  
![Screenshot](screenshot.png)

Setup
-----
`git clone --recursive https://gitlab.com/mikenew/launchbot.git`

copy `config_example.ini` to `config.ini`  

Create a new matrix user that you'd like to be the bot. 
You could create one on the matrix.org server [here](https://app.element.io/#/register), for example. 
Name it something appropriate (i.e. `launchbot`) 
and invite it to the room where you'd like to get notifications.

You'll need to put the bot's access token in `config.ini`.  
In Element, you can get the token by going to 
Settings -> Help & About -> Advanced -> Access Token.

You'll also need to put the room ID in `config.ini`.  
In Element: Room Settings (click on the name of the room to reveal the dropdown) -> Advanced -> Room Information. 
This can be a comma-separated list if you'd like to send messages to multiple rooms

Change the baseUrl in `config.ini` as well. 
This is just your server address. 
If you're using matrix.org it would be `https://matrix.org`

Compiling and Running
---------------------
[Install Nim](https://nim-lang.org/install.html)  
compile: `nim c launchbot.nim`  
run: `./launchbot`
