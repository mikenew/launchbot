import std/[httpclient, json, strutils, sequtils, sugar, times, parsecfg, uri, asyncdispatch]
import matrix-nim-bot/bot

# Launch Library 2 API
# https://thespacedevs.com/llapi

var config = loadConfig("./config.ini")
var matrixRoomIds = config.getSectionValue("", "roomIds").split(",").map(x => x.strip().encodeUrl())
var matrixToken = config.getSectionValue("", "token")
var matrixUrl = config.getSectionValue("", "baseUrl")

const useDev = false # dev url doesn't have rate limits. Useful for debugging
const baseURL = if useDev: "https://lldev.thespacedevs.com/2.0.0" else: "https://ll.thespacedevs.com/2.0.0"
let client = newHttpClient()

const notifyWindow = 1 # one minute window for notifications

proc getNow(): DateTime =
  #return now() + 2.days + 5.hours + 15.minutes # use for testing
  return now()

type Launch = object
  name: string
  date: DateTime
  rocket: string
  location: string
  stream: string
  id: string

  #minutes for all of these
  wantedNotifications: seq[int]
  performedNotifications: seq[int]
  wantedDetailedUpdates: seq[int]
  performedDetailedUpdates: seq[int]


proc loadWantedNotifications(l: var Launch) =
  # Determines if we want pre-launch notifications ("1 hour until falcon 9 launch", for example)
  # We'll still get daily notifications for these

  let suppressed = ["proton-m", "kuaizhou", "long march", "soyuz", "ariane"]
  let noisy = ["new shepard", "falcon", "launcherone", "electron", "starship", "neutron"]

  var wanted = @[16] # default

  for s in suppressed:
    if l.name.toLower.contains(s.toLower): 
      wanted = @[]
      break

  for s in noisy:
    if l.name.toLower.contains(s.toLower): 
      wanted = @[16, 61]
      break

  l.wantedNotifications = wanted


proc launch(json: JsonNode): Launch =
  result.name = json["name"].getStr
  result.date = parse(json["net"].getStr, "yyyy-MM-dd'T'HH:mm:ss'Z'", utc())
  result.rocket = json{"rocket"}{"configuration"}{"name"}.getStr("")
  result.location = json{"pad"}{"location"}{"name"}.getStr("")
  result.id = json{"id"}.getStr("")
  loadWantedNotifications(result)
  result.wantedDetailedUpdates = @[18, 33, 63]

proc prettyDateTime(l: Launch): string =
  result = l.date.local.format("M-dd-yy h:mmtt")

proc shortLocation(l: Launch): string =
  result = l.location.split(",")[0]

proc prettyTime(l: Launch): string =
  result = l.date.local.format("h:mmtt") & " PST"

proc prettyTimeUntil(l: Launch): string =
  let t = getNow().between(l.date)
  if t.hours > 0 and t.minutes == 0:
    if t.minutes > 0:
      result = $t.hours & ":" & $t.minutes
    else:
      result = $t.hours & (if t.hours == 1: " hour" else: " hours")
  else:
    result = $t.minutes & " minutes"

proc launchIn(l: Launch, time: int): bool =
  return l.date < (getNow() + time.minutes)

proc update(l: var Launch, json: JsonNode) =
  if json{"name"} != nil: l.name = json["name"].getStr
  if json{"net"} != nil: l.date = parse(json["net"].getStr, "yyyy-MM-dd'T'HH:mm:ss'Z'", utc())
  if json{"rocket"}{"configuration"}{"name"} != nil: l.rocket = json["rocket"]["configuration"]["name"].getStr
  if json{"pad"}{"location"}{"name"} != nil: l.location = json["pad"]["location"]["name"].getStr
  if json{"vidURLs"}{0}{"url"} != nil: l.stream = json["vidURLs"][0]["url"].getStr

  if l.stream != "": echo "updated with stream link: " & l.stream

proc merge(launches: var seq[Launch], json: seq[JsonNode]) =
  var additions: seq[Launch] = @[]

  for l in json:
    var matched = false

    for existing in launches.mitems:
      if l{"id"}.getStr("") == existing.id:
        existing.update(l)
        matched = true
        break

    if not matched:
      additions.add(launch(l))

  launches.add(additions)
  for l in additions:
    echo "new launch added: " & l.rocket & " launching from " & l.shortLocation() & " at " & l.prettyDateTime() & " id: " & l.id

proc getLaunchListJson(): seq[JsonNode] =
  let url = baseURL & "/launch/upcoming/"
  try:
    let response = client.request(url, httpMethod = HttpGet)
    let json = parseJson(response.body)
    if json{"results"} != nil:
      for r in json["results"]:
        result.add(r)
  except:
    echo "Error connecting to Launch Library API: " & getCurrentExceptionMsg()

proc getLaunchJson(id: string): seq[JsonNode] =
  let url = baseURL & "/launch/" & id & "/"
  try:
    let response = client.request(url, httpMethod = HttpGet)
    let json = parseJson(response.body)
    if json{"id"}.getStr("") == id: result = @[json]
  except:
    echo "Error connecting to Launch Library API: " & getCurrentExceptionMsg()


var launches: seq[Launch] = @[]

proc notifyViaMatrix(l: Launch) =
  var m = l.rocket & " launching from " & l.shortLocation() & " in " & l.prettyTimeUntil()
  if l.stream != "": m = m & "<br>" & l.stream

  bot.sendMessageToMultiple(m, true, matrixUrl, matrixRoomIds, matrixToken)

proc notifyForLaunchesIfNeeded() =
  for i in 0..<launches.len():
    var l = launches[i]

    for t in l.wantedNotifications:
      if t notin l.performedNotifications and l.launchIn(t) and not l.launchIn(t - notifyWindow):
        l.notifyViaMatrix()
        l.performedNotifications.add(t)

    launches[i] = l

var lastDailyNotification = getNow() - 100.days

proc dailyNotificationIfNeeded() =
  var todayLaunches: seq[Launch] = @[]

  if getNow().hour == 5 and getNow().minute == 0:
    let elapsed = lastDailyNotification.between(getNow())
    if elapsed.hours > 1 or elapsed.days > 0:
      lastDailyNotification = getNow()
      for l in launches:
        if l.launchIn(60 * 24) and not l.launchIn(0):
          todayLaunches.add(l)

  if todayLaunches.len > 0:
    var m = "<b>Launching Today</b><br>"
    for l in todayLaunches:
      m = m & l.rocket & " launching from " & l.shortLocation() & " at " & l.prettyTime()
      if l.stream != "": m = m & "<br>" & l.stream
      m = m & "<br>"

    bot.sendMessageToMultiple(m, true, matrixUrl, matrixRoomIds, matrixToken)


proc updateLaunchData(interval: int) {.async.} =
  while true:
    launches.merge(getLaunchListJson())
    await sleepAsync(interval)

proc notifyAndDetailsLoop(interval: int) {.async.} =
  while true:
    notifyForLaunchesIfNeeded()
    dailyNotificationIfNeeded()

    # update with launch details (link to stream, etc.) if needed
    for i in 0..<launches.len():
      var l = launches[i]

      for t in l.wantedDetailedUpdates:
        if t notin l.performedDetailedUpdates and l.launchIn(t) and not l.launchIn(t - notifyWindow):
          launches.merge(getLaunchJson(l.id))
          echo "fetched detailed update for " & l.name
          launches[i].performedDetailedUpdates.add(t)

      await sleepAsync(1000 * 2) # wait a few seconds so we don't hammer the server and get throttled

    await sleepAsync(interval)


# async update loops. will exit if main thread exits
# 20 minute intervals (limit is 300 API requests per day, this will be 72)
asyncCheck updateLaunchData(1000 * 60 * 20) 
# 20 second intervals. Only updates or sends notifications if needed
asyncCheck notifyAndDetailsLoop(1000 * 20) 

echo "current time is: " & getNow().local.format("M-dd-yy h:mm:ss tt")

#bot.sendMessageToMultiple("Hello I'm alive!", true, matrixUrl, matrixRoomIds, matrixToken)

runForever() # main thread never exits
